package no.ntnu.app.firstaid;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Represents a springboot test. Not used to be honest at this moment.
 */
@SpringBootTest
class theAidApplicationTests {
	/**
	 * Empty test. not used at all.
	 */
	@Test
	void contextLoads() {
	}

}
