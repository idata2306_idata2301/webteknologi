let selectedLanguage;
let selectedNoOfPeople;
let selectedDate;
let selectedTimeOfTheDay;
let basket = [];
let enrolledCourses = 0;
let count = 0 ;
const cartDiv  = document.querySelector(".cart");


//Displays when no course is enrolled.
const html = `
        <div>
          <p>No courses enrolled yet !</p>     
        </div>
     `;
cartDiv.insertAdjacentHTML("beforeend", html);

// Displays the given data in course div container in course.html template
const setCourses = function (data) {
    const html = `
        <div class="course-display">
            <p class="course-name">${data.name}</p>
            <p class="course-page-description">${data.description}</p>
            <p class="course-price"> ${data.price} kr</p>
          
             <select name="language" id="language" class="options" onchange="getLanguage(this)" required>
                 <option value=" ">Language</option>
                 <option value="ENGLISH">English</option>
                 <option value="NORWEGIAN">Norwegian</option>
             </select> 
              <select name="people" id="people" class="options" onchange="getPeople(this)" required>
                 <option value=" ">No of people</option>
                 <option value="ONE">1</option>
                 <option value="FIVE">5</option>
             </select> 
             <select name="timeOfTheDay" id="time-of-day" class="options" onchange="getTimeOfTheDay(this)" required>
                 <option value=" ">When</option>
                 <option value="MORNING">Morning</option>
                 <option value="EVENING">Evening</option>
             </select> 
                   
             <select name="date" id="date" class="options" onchange="getDate(this)" required>
                 <option value=" ">Date</option>
                 <option value="06/06/2022">06/06/2022</option>
                 <option value="07/07/2022">07/07/2022</option>
                 <option value="06/08/2022">06/08/2022</option>
                 <option value="07/09/2022">07/09/2022</option>
             </select> 
             <p class="height-fix"></p>
            <button type="submit" class="enroll-btn" onclick="enroll(${data.id})">Enroll</button>          
    </div>
     `;
    document.querySelector(".course-container").insertAdjacentHTML("beforeend", html);
};
//Gets the selected value of language.
function getLanguage(selected) {
    selectedLanguage = selected.options[selected.selectedIndex].value;
}
//Gets the selected value of people.
function getPeople(selected) {
    selectedNoOfPeople = selected.options[selected.selectedIndex].value;
}
//Gets the selected value of date.
function getDate(selected) {
    selectedDate = selected.options[selected.selectedIndex].value;
}
//Gets the selected value of time of the day.
function getTimeOfTheDay(selected) {
    selectedTimeOfTheDay = selected.options[selected.selectedIndex].value;
}

// Gets all the courses from DB and calls setCourse() to display it.
const getCourseData = function () {
    fetch(`/courses`)
        .then(response => response.json())
        .then(data => {
            for(let i = 0; i<data.length; i++){
                setCourses(data[i]);
            }
        });
};
getCourseData();


// Will be called when enroll button is clicked
function enroll(id){
    fetch(`/isSecured`)
        .then(response => response.json())
        .then(data => {
          if(!data){
              alert("Please login to enroll in courses.")
              window.location.pathname = '/login';
          }
        });
    basket.push(id);
    if(enrolledCourses===0){
        cartDiv.innerHTML="";
    }
    enrolledCourses++;

    //This clears the message in the cart section, when customer enrolls in the first course.
    if(count === 0){
        cartDiv.innerHTML="";
        count = count + 1;}
    //Displays the confirm button in the cart section, when customer enrolls in the first course.
    if(count === 1){
        const html = `
        <button class="book-now confirm-btn" onClick="confirm()"> Confirm order </button>
          `;
        cartDiv.insertAdjacentHTML("afterend", html);
        count = count + 1;}
    //Displays the item in the cart section.
    displayInCart(id)

    if(enrolledCourses>0){
        document.querySelector(".confirm-btn").
        removeAttribute("hidden");
    }
}
//displays the enrolled courses
function displayInCart(id){
    fetch(`/show-courses/${id}`)
        .then(response => response.json())
        .then(data => {
            setEnrolledCourses(data);
        });
}

// Displays the given data in cart section.
const setEnrolledCourses = function (data) {
    const html = `
        <div class="cart-display">
            <p class="cart-display-name">${data.name}</p>
            <p>  ${data.price} kr</p>
            <button type="button" class="danger" onfocus="removeEnrolledCourse(${data.id})">Remove</button>          
    </div>
     `;
    cartDiv.insertAdjacentHTML("beforeend", html);
};

//Removes the course from cart section when remove button is clicked.
function removeEnrolledCourse(id){
    let index = basket.indexOf(id);
    if (index > -1) { basket.splice(index, 1) }

    let rmv = document.getElementsByClassName("danger")
    for(let i=0; i<rmv.length; i++) {
        let btnChosen = rmv[i]
        btnChosen.addEventListener(`click`, function (event) {
            let btnClicked = event.target;
            btnClicked.parentElement.remove();

        })
    }
    enrolledCourses--;

    if(enrolledCourses===0){
        document.querySelector(".confirm-btn").
        setAttribute("hidden","true");
        cartDiv.insertAdjacentHTML("beforeend", html);
    }
}
// Enrolls course
function enrollInCourse(customerId,courseId){

            let data = {
                customerId : customerId,
                courseId : courseId,
                language : selectedLanguage,
                people : selectedNoOfPeople,
                date : selectedDate,
                timeOfTheDay : selectedTimeOfTheDay
            }

            fetch(`/save-enrolled-courses`,{
                method:"POST",
                body: JSON.stringify(data),
                headers: {"Content-Type" : "application/json; charset=UTF-8"}
            })
                .then(response => response.json())
                .then(data => console.log(data))
                .catch(err => console.log("the error msg " + err));
}

//Saves the order details in the db and redirected to the account details page.
async function confirm() {
    await fetch(`/current-user-id`)
        .then(response => response.json())
        .then(customerId => {
            for (let i = 0; i < basket.length; i++) {
                enrollInCourse(customerId, basket[i])
            }
             alert("Enrolled successfully!")
             window.location.pathname = '/customer-details';
        });
}


