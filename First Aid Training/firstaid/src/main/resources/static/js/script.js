"use strict";


/* HAMBURGER MENU - Responsive design */

/**
 * Adding event listeners to all elements in the in the hamburger drop-down menu.
 * This method is borrowed online from a youtube tutorial : https://www.youtube.com/watch?v=flItyHiDm7E
 *
 * @type {Element}
 */
const hamburger = document.querySelector(".hamburger");
const headerNav = document.querySelector(".header-nav");

/* when the burger menu is clicked */
hamburger.addEventListener("click", () => {
    hamburger.classList.toggle("active");
    headerNav.classList.toggle("active");
});

document.querySelectorAll("nav-link").forEach(n => n.addEventListener("click", () => {
    hamburger.classList.remove("active");
}));

