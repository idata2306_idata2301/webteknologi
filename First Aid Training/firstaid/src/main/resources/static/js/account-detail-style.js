"use strict";

// Stores the id of the current logged-in user.
let currentUserId;

// Element will be displayed when there is no courses enrolled.
const html = `
              <p class="enrolled-courses-null">You are not enrolled in any courses !</p>
              `;

// Displaying single user
const singleUserContainerInTable = document.querySelector(".account-details-table-single");
const myCourseDisplay = document.querySelector(".my-courses-display")
const myReviewDisplay = document.querySelector(".my-reviews-display")

// Displays the information about the logged-in user.
const setSingleUserToTable = function (data) {
    const htmlCustomer = `
        <div class="single-user-table">
        <h1 class="hello-user">Hello, ${data.firstName} ${data.lastName}!</h1>
        <table>
        <tr>
            <td class="label">First name:</td>
            <td>${data.firstName}</td>
        </tr>
        <tr>
            <td class="label">Last name:</td>
            <td>${data.lastName}</td>
        </tr>
        <tr>
            <td class="label">Email:</td>
            <td>${data.email}</td>
        </tr>
    </table>
     <button id="update" type="submit" onclick="updateCustomer()">Update</button>
    </div>
     `;
    singleUserContainerInTable.insertAdjacentHTML("beforeend", htmlCustomer);
};

// Updates the customer profile.
function updateCustomer() {
    document.getElementById("update").setAttribute("disabled","true");
    fetch(`/customers/${currentUserId}`)
        .then(response => response.json())
        .then(customer => {
            const htmlUpdate = `
             <h2 class="my-update">Update profile</h2>
             <form  class="update-container" autocomplete="off" onsubmit="event.preventDefault()">
                <input id="id" value="${customer.id}" hidden  />
                <input id="firstName" type="text" value="${customer.firstName}" required  />
                <input id="lastName" type="text" value="${customer.lastName}" required  />
                <input id="password" type="password" value="" required />
                <input id="email"  value="${customer.email}" hidden  />
                <button id="button-save" onclick="updateCustomerInDb()">update</button>
                <button id="button-cancel" onclick="cancel()">cancel</button>
             </form>
            
              `;
            document.querySelector(".update-customer").insertAdjacentHTML("beforeend", htmlUpdate);
        });
}

// Redirects to customersList account page when customer cancels updating of profile.
function cancel(){
    window.location.pathname = '/customer-details';
}

// Stores the updated customer in the db
function updateCustomerInDb(){
    let updatedId = document.getElementById("id").value;
    let updatedFirstName = document.getElementById("firstName").value;
    let updatedLastName = document.getElementById("lastName").value;
    let  updatedPassword= document.getElementById("password").value;
    let updatedEmail = document.getElementById("email").value;
    //when fields are empty
    if(updatedFirstName.length ===0 || updatedLastName.length === 0 || updatedPassword.length === 0){
        alert("Please fill in all the details !");
        window.location.pathname = '/customer-details';
        return;
    }

    let updatedCustomer = {
        id : updatedId,
        firstName : updatedFirstName,
        lastName : updatedLastName,
        password : updatedPassword,
        email : updatedEmail
    }

    fetch(`/updateCustomer`,{
        method:"PUT",
        headers: {"Content-Type" : "application/json; charset=UTF-8"},
        body: JSON.stringify(updatedCustomer)
    })
        .then(response => response.json())
        .then(data => {
            alert("You have successfully updated your profile details. Please login again to update the changes !")
            window.location.pathname = '/logout';
        })
        .catch(err => console.log("the error msg " + err));
}

// Gets the customer logged in by id.
const getCustomerData = function (id) {
    fetch(`/customers/${id}`)
        .then(response => response.json())
        .then(data => {
            setSingleUserToTable(data)
        });
};

// Displays the enrolled courses of the customer
const setCustomerEnrolledCourses = function (data) {
    fetch(`/show-courses/${data.courseId}`)
        .then(response => response.json())
        .then(courseFound => {
    const html = `
    <div class="enrolled-courses">
         <p>${courseFound.name}</p>
         <p>${courseFound.description}</p>
          <button type="button" class="remove" onclick="remove(${data.id})">Remove</button>
         <p>-------------------------------</p>   
    </div>
    `;
    myCourseDisplay.insertAdjacentHTML("beforeend",html);
});
}

// Deletes a course from enrolled courses
function remove(enrolledId){
    fetch(`/enrolled-courses/${enrolledId}`,
        {
            method: "DELETE",
            headers: {"Content-Type": "application/json; charset=UTF-8"}
        })
        .then(response => {
            document.querySelector(".remove").parentElement.remove();
                let elementCount = document.querySelector(".my-courses-display").
                querySelectorAll(".enrolled-courses").length;
                if(elementCount===0){
                    myCourseDisplay.insertAdjacentHTML("beforeend",html);
                }
        }
        );

}

// Gets all the enrolled courses.
const getCustomerCourseData = async function (id) {
      await fetch(`/enrolled-courses/${id}`)
        .then(response => response.json())
        .then(data => {
           if(data.length === 0){
                myCourseDisplay.insertAdjacentHTML("beforeend",html);
                return;
            }
            for(let i=0; i<data.length;i++){
                setCustomerEnrolledCourses(data[i]);
            }
        });
}


// Gets the reviews written.
const getReviewsWritten = async function (id) {
    await fetch(`/customers/${id}/reviews`)
        .then(response => response.json())
        .then(reviewsFound => {
            for(let i=0; i<reviewsFound.length;i++){
                console.log(reviewsFound)
                const review = `
                <div class="enrolled-courses">
                   <p>${reviewsFound[i].review}</p>
                   <p>-------------------------------</p>   
                </div>
                `;
                myReviewDisplay.insertAdjacentHTML("beforeend",review);
            }

        });
}


//Getting current logged-in user info to display user details, courses enrolled and reviews.
const getCustomerInfo = function () {
    fetch(`/current-user-id`)
        .then(response => response.json())
        .then(data => {
            currentUserId = data;
            getCustomerData(data);
            getCustomerCourseData(data);
            getReviewsWritten(data);
        });
};
getCustomerInfo();






