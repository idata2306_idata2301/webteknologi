package no.ntnu.app.firstaid.entity;

/**
 * The languages available for the courses.
 */
public enum Languages {
    ENGLISH,
    NORWEGIAN
}
