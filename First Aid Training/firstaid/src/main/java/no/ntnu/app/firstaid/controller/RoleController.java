package no.ntnu.app.firstaid.controller;

import no.ntnu.app.firstaid.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
/**
 * Controller that handles the role endpoints.
 */
@CrossOrigin(originPatterns = "*")
@RestController
public class RoleController {
    /**
     * The role service.
     */
    @Autowired
    private RoleService roleService;

}
