package no.ntnu.app.firstaid.security;

import no.ntnu.app.firstaid.entity.Customer;
import no.ntnu.app.firstaid.repo.CustomerRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Represents a service to be used for the customer details class.
 */
@Service
public class  MyCustomerDetailService implements UserDetailsService {
    /**
     * The customer repository.
     */
    @Autowired
    CustomerRepo customerRepo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Customer> foundCustomer = customerRepo.findCustomerByEmail(email);
        return foundCustomer.map(MyCustomerDetails::new).get();

    }
}
