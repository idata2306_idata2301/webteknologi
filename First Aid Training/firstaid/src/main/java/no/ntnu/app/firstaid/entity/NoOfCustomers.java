package no.ntnu.app.firstaid.entity;

/**
 * The number of people that can join a course.
 */
public enum NoOfCustomers {
    ONE,
    FIVE
}
