package no.ntnu.app.firstaid.entity;

import javax.persistence.*;

/**
 * Represents the reviews written by the customers.
 */
@Entity
public class Reviews {
    /**
     * The id of the review.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * The review itself.
     */
    private String review;
    /**
     * The customer writing the review.
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    private Customer customer;

    /**
     * Represents a review.
     * @param id of the review.
     * @param review itself.
     */
    public Reviews(int id, String review) {
        this.id = id;
        this.review = review;
    }

    /**
     * Left blank as intended.
     */
    public Reviews() {
    }

    /**
     * Returns the id of the review.
     * @return the id of the review.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the review.
     * @param id of the review.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the review.
     * @return the review.
     */
    public String getReview() {
        return review;
    }

    /**
     * Sets the review.
     * @param review to be set.
     */
    public void setReview(String review) {
        this.review = review;
    }

    /**
     * Assigns a customer to the review.
     * @param customer to be assigned.
     */
    public void assignCustomer(Customer customer){
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Reviews{" +
                "id=" + id +
                ", review='" + review + '\'' +
                ", customer=" + customer +
                '}';
    }
}

