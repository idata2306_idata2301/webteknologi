package no.ntnu.app.firstaid.repo;

import no.ntnu.app.firstaid.entity.Customer;
import no.ntnu.app.firstaid.entity.Reviews;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
/**
 * Represents the reviews repository.
 */
public interface ReviewsRepo extends CrudRepository<Reviews,Integer> {
    Optional<Reviews> findById(int id);
}
