package no.ntnu.app.firstaid.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Represents a security configuration.
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * The user details service.
     */
    @Autowired
    UserDetailsService userDetailsService;

    //Authentication
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    /**
     * Returns the BCryptPassworEncoder.
     * @return the BCryptPassworEncoder.
     */
    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
        .authorizeRequests()
                .antMatchers("HttpMethod.POST").permitAll()
                .mvcMatchers("/registrationform").permitAll()
                .mvcMatchers("/save-customer").permitAll()
                .mvcMatchers("/customer-details").hasAnyRole("USER","ADMIN")
                .mvcMatchers("/save-review").hasAnyRole("USER","ADMIN")
                .antMatchers("/enrolled-courses","/enrolled-courses/").hasRole("ADMIN")
                .antMatchers("save-enrolled-courses").hasAnyRole("USER","ADMIN")
                .antMatchers("/enrolled-courses/*").hasAnyRole("USER","ADMIN")
                .antMatchers("/customers","customers/").hasRole("ADMIN")
                .antMatchers("/admin-details").hasRole("ADMIN")
                .antMatchers("/customers/**").hasAnyRole("USER","ADMIN")
                .antMatchers("/reviews").hasRole("ADMIN")
                .antMatchers("/").permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .and ()
                .formLogin()
                .permitAll()
                .loginPage("/login")
                .usernameParameter("email");
    }
}
