package no.ntnu.app.firstaid.service;

import no.ntnu.app.firstaid.repo.CourseRepo;
import no.ntnu.app.firstaid.entity.Course;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Represents a service to be used for the course.
 */
@Service
public class CourseService {
    private final CourseRepo courseRepo;

    /**
     * Initializes the corse service class.
     * @param courseRepo to be initialized.
     */
    public CourseService(CourseRepo courseRepo) {
        this.courseRepo = courseRepo;
    }

    /**
     * Returns all the courses.
     * @return all the courses.
     */
    public List<Course> getAllCourses() {
        return (List<Course>) courseRepo.findAll();
    }

    /**
     * Returns the course by id.
     * @param id of the course.
     * @return course by the id.
     */
    public Course getCourseById(int id) {
        return courseRepo.findById(id).orElse(null);
    }

    /**
     * Adds a course to the course service.
     * @param course to be added.
     * @return <code>TRUE</code> if added correctly, else <code>FALSE</code> will be returned.
     */
    public boolean addCourse(Course course) {
        boolean response = false;
        if(course.getName().isBlank()||course.getDescription().isBlank()){
            return false;
        }
        if(course!=null){
            courseRepo.save(course);
            response = true;
        }
        return response;
    }
}
