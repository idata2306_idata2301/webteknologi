package no.ntnu.app.firstaid.service;

import no.ntnu.app.firstaid.entity.Customer;
import no.ntnu.app.firstaid.entity.Reviews;
import no.ntnu.app.firstaid.repo.ReviewsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Represents a service to be used for the reviews.
 */
@Service
public class ReviewsService {
    /**
     * Represents the review repository.
     */
    @Autowired
    private ReviewsRepo reviewsRepo;
    /**
     * Represents the customer service.
     */
    @Autowired
    private CustomerService customerService;

    /**
     * Adds the given review to the database. Assigns the customer to the review.
     * @param reviews review to be added.
     * @return <code>true</code> if added successfully,
     *         <code>false</code> otherwise.
     */
    public boolean addReview(Reviews reviews){
        if(reviews!=null){
            reviewsRepo.save(reviews);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            int currentUser = customerService.getCustomerIdFromUserName(authentication.getName());
            Customer customer = customerService.getCustomerById(currentUser);
            reviews.assignCustomer(customer);
            reviewsRepo.save(reviews);
            return true;
        }
        return false;
    }

    /**
     * Edits a given review.
     * @param reviews review to be edited.
     * @return <code>true</code> if edit is saved successfully,
     *         <code>false</code> otherwise.
     */
    public boolean editReview(Reviews reviews){
        boolean response = false;
        if(reviews!=null){
            reviewsRepo.save(reviews);
            response = true;
        }
        return response;
    }

    /**
     * Deletes the given review
     * @param reviewsId id of the review
     * @return <code>true</code> if review is deleted successfully.
     *         <code>false</code> otherwise.
     */
    public boolean deleteReview(int reviewsId){
        Reviews review = reviewsRepo.findById(reviewsId).orElse(null);
        if(review!=null){
            reviewsRepo.delete(review);
            return true;
        }
        return false;
    }

    /**
     * Gets list of reviews written by the customers.
     * @return list of customers.
     */
    public List<Reviews> getAllReviews(){
        return (List<Reviews>) reviewsRepo.findAll();
    }

}
