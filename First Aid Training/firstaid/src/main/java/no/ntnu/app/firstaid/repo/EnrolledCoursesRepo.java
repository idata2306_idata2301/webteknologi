package no.ntnu.app.firstaid.repo;

import no.ntnu.app.firstaid.entity.EnrolledCourses;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
/**
 * Represents the enrolled course repository.
 */
public interface EnrolledCoursesRepo extends CrudRepository<EnrolledCourses,Integer> {
    List<EnrolledCourses> findByCustomerId(int id);
}
