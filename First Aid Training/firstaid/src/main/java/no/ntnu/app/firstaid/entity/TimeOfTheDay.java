package no.ntnu.app.firstaid.entity;

/**
 * Represents the time of the day where a course can be held.
 */
public enum TimeOfTheDay {
    MORNING,
    EVENING
}
