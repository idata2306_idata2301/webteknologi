package no.ntnu.app.firstaid.repo;

import no.ntnu.app.firstaid.entity.Role;
import org.springframework.data.repository.CrudRepository;
/**
 * Represents the role repository.
 */
public interface RoleRepo extends CrudRepository<Role,Integer> {
}
