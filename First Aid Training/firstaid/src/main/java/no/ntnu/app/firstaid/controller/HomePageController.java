package no.ntnu.app.firstaid.controller;


import no.ntnu.app.firstaid.entity.Reviews;
import no.ntnu.app.firstaid.repo.EnrolledCoursesRepo;
import no.ntnu.app.firstaid.service.CustomerService;
import no.ntnu.app.firstaid.entity.Customer;
import no.ntnu.app.firstaid.service.ReviewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
/**
 * Controller that handles the home page endpoints.
 */
@Controller
public class HomePageController {
    /**
     * The customer service.
     */
    @Autowired
    private CustomerService customerService;
    /**
     * The review service.
     */
    @Autowired
    private ReviewsService reviewsService;
    /**
     * The enrolled courses repository.
     */
    @Autowired
    EnrolledCoursesRepo enrolledCoursesRepo;

    /**
     * Gets the home page.
     * @return the home page.
     */
    @GetMapping("")
    public String getHomePage() {
        return "index";
    }

    /**
     * Gets all the details accessible by a user with admin role.
     * @param model an instance of Model.
     * @return admin-details page which have access to customers list.
     */
    @GetMapping("/admin-details")
    public String getCustomers(Model model) {
        model.addAttribute("customersList", customerService.getAllCustomers());
        return "admin-details";
    }

    /**
     * Gets the registration form for the customer.
     * @param model instance of Model.
     * @return registration page which have access to customer object.
     */
    @GetMapping("/registrationform")
    public String addCustomerForm(Model model) {
        model.addAttribute("customer", new Customer());
        return "registration";
    }

    /**
     * Adds new customer to the database.
     * @param customer customer to be added.
     * @param bindingResult stores error messages from the registration page.
     * @return <code>success-page</code> if customer is added successfully,
     *         <code>registration</code> if inputs are invalid.
     */
    @PostMapping("/save-customer")
    public String saveCustomer(@Valid @ModelAttribute Customer customer,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if(customerService.addCustomer(customer)){  return "new-customer-success";}
        return "email-already-exists";
    }

    /**
     * Redirects to page according to authentication.
     * @return <code>login</code> page if customer is not authenticated,
     *         <code>index</code> otherwise.
     */
    @GetMapping("/login")
    public String showLogin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null || authentication instanceof AnonymousAuthenticationToken){
            return "login";
        }
        return "index";
    }

    /**
     * Gets customer details page with profile and course information.
     * @return customer-details page.
     */
    @GetMapping("/customer-details")
    public String accountDetails() {
        return "customer-details";
    }

    /**
     * Displays all the courses available in the database.
     * @return courses page with all the courses available.
     */
    @GetMapping("/show-courses")
    public String showCourses(){
        return "courses";
    }

    /**
     * Displays the review input page.
     * @param model instance of Model containing reviews instance.
     * @return reviews-page.
     */
    @GetMapping("/review-form")
    public String showReviewForm(Model model) {
        model.addAttribute("reviews", new Reviews());
        return "review";
    }

    /**
     * Adds a review to the database. Customer logged in will be assigned to the review.
     * @param reviews review written.
     * @return success-page.
     */
    @PostMapping("/save-review")
    public String addReviewForm(@ModelAttribute Reviews reviews) {
        reviewsService.addReview(reviews);
        return "/review-success";
    }
}