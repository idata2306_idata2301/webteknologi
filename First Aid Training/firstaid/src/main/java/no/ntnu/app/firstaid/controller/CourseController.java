package no.ntnu.app.firstaid.controller;

import no.ntnu.app.firstaid.service.CourseService;
import no.ntnu.app.firstaid.entity.Course;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller that handles the course endpoints.
 */
@CrossOrigin(originPatterns = "*")
@RestController
public class CourseController {
    private CourseService courseService;

    /**
     * Creates a new instance of courseController.
     * @param courseService to be used.
     */
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    /**
     * Adds a course to the database.
     * @param course course to be added.
     * @return <code>HttpStatus.OK</code> if added successfully,
     *         <code>HttpStatus.NOT_ACCEPTABLE</code> otherwise.
     */
    @PostMapping("/courses")
    public HttpStatus addCourse(@RequestBody Course course){
        if(courseService.addCourse(course)) {
            return HttpStatus.OK;
        }
        return HttpStatus.NOT_ACCEPTABLE;
    }

    /**
     * Gets all the courses available.
     * @return list of courses.
     */
    @GetMapping("/courses")
    public List<Course> getAllCourses(){
        return courseService.getAllCourses();
    }

    /**
     * Gets a course by id.
     * @param id id of the course to be found.
     * @return <code>course</code> found.
     */
    @GetMapping("/show-courses/{id}")
    public Course getCourseById(@PathVariable int id){
        return courseService.getCourseById(id);
    }

}
