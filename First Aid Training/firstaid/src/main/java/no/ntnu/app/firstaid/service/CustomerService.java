package no.ntnu.app.firstaid.service;

import no.ntnu.app.firstaid.entity.Customer;
import no.ntnu.app.firstaid.entity.Reviews;
import no.ntnu.app.firstaid.entity.Role;
import no.ntnu.app.firstaid.repo.CustomerRepo;
import no.ntnu.app.firstaid.repo.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

/**
 * Represents a service to be used for the customer.
 */
@Service
public class CustomerService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    private final CustomerRepo customerRepo;
    private final RoleRepo roleRepo;

    /**
     * Creates an instance of CustomerService
     * @param customerRepo an instance of CustomerRepo
     * @param roleRepo an instance of RoleRepo
     */
    public CustomerService(CustomerRepo customerRepo, RoleRepo roleRepo) {
        this.customerRepo = customerRepo;
        this.roleRepo = roleRepo;
    }

    /**
     * Adds a new customer to the database.
     * @param customer new customer to add.
     * @return <code>true</code> if customer is saved successfully,
     *         <code>false</code> if customer is not saved.
     */
    public boolean addCustomer(Customer customer){
        boolean response = false;
        if(customer!=null && customer.emailValidator(customer.getEmail())){
            //checks for existing customersList.
            if(customerRepo.findCustomerByEmail(customer.getEmail()).isPresent()){
                return false;
            }
            customer.setPassword(passwordEncoder.encode(customer.getPassword()));
            customerRepo.save(customer);
            addDefaultRoleToCustomer(customer);
            response = true;
        }
        return response;
    }

    /**
     * Adds customer role according to the email address.
     * @param customer to set role.
     * @return <code>true</code> if <code>role</code> is added successfully,
     *          otherwise <code>false</code>
     */
    public boolean addDefaultRoleToCustomer(Customer customer){
        Role role;
        if(customer!=null) {
            if (customer.getEmail().contains("@ntnu.no")) {
                role = roleRepo.findById(2).orElse(null);
            } else {
                role = roleRepo.findById(1).orElse(null);
            }
            customer.addRoleToCustomer(role);
            customerRepo.save(customer);
            return true;
        }
        return false;
    }

    /**
     * Gets list of all customer from database.
     * @return list of customersList.
     */
    public List<Customer> getAllCustomers() {
        return (List<Customer>) customerRepo.findAll();
    }

    /**
     * Gets a customer matching the given customer id.
     * @param id id of the customer
     * @return <code>customer</code> if present, otherwise <code>null</code>.
     */
    public Customer getCustomerById(int id){
        return customerRepo.findById(id).orElse(null);
    }


    /**
     * Deletes the customer from the database.
     * @param id id of the customer.
     * @return <code>true</code> if customer is deleted successfully,
     *          otherwise <false>false</false>
     */
    public boolean deleteCustomer(int id) {
        Customer foundCustomer = getCustomerById(id);
        if(foundCustomer!=null){
            customerRepo.delete(foundCustomer);
            return true;
        }
        return false;
    }

    /**
     * Adds a role to the given customer
     * @param customerId id of the customer
     * @param roleId id of the role
     * @return <code>true</code> if role assigned successfully,
     *         <code>false</code> otherwise.
     */
    public boolean addRoleToCustomer(int customerId, int roleId){
        Customer customerFound = customerRepo.findById(customerId).orElse(null);
        Role roleFound = roleRepo.findById(roleId).orElse(null);
        if(customerFound!=null && roleFound!=null){
            customerFound.addRoleToCustomer(roleFound);
            customerRepo.save(customerFound);
            return true;
        }
        return false;
    }

    /**
     * Updates an existing customer.
     * @param customer <code>customer</code> to update.
     * @return <code>true</code> if updated successfully,
     *         <code>false</code> otherwise.
     */
    public boolean updateCustomer(Customer customer) {
        if(customer!=null && customer.emailValidator(customer.getEmail())){
            customer.setPassword(passwordEncoder.encode(customer.getPassword()));
            customerRepo.save(customer);
            addDefaultRoleToCustomer(customer);
            return true;
        }
        return false;
    }

    /**
     * Get the id of the given username
     * @param userName username of the customer
     * @return if the given username is found, returns the id of the user,
     *         otherwise -1.
     */
    public int getCustomerIdFromUserName(String userName){
        Customer foundCustomer = customerRepo.findCustomerByEmail(userName).orElse(null);
        if(foundCustomer!=null){
            return foundCustomer.getId();
        }
        return -1;
    }

    /**
     * Gets list of reviews written by the logged-in user.
     * @param customerId id of the customer
     * @return list of reviews
     */
    public List<Reviews> getReviewsByCustomerId(int customerId) {
        Customer customer = getCustomerById(customerId);
        if(customer!=null){
            return customer.getReviews();
        }
        return Collections.emptyList();
    }
}
