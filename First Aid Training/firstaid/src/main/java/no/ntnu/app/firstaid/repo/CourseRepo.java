package no.ntnu.app.firstaid.repo;

import no.ntnu.app.firstaid.entity.Course;
import org.springframework.data.repository.CrudRepository;

/**
 * Represents the course repository.
 */
public interface CourseRepo extends CrudRepository<Course,Integer> {
}
