package no.ntnu.app.firstaid.controller;

import no.ntnu.app.firstaid.entity.Reviews;
import no.ntnu.app.firstaid.service.CustomerService;
import no.ntnu.app.firstaid.service.ReviewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
/**
 * Controller that handles the reviews endpoints.
 */
@CrossOrigin
@RestController
public class ReviewsController {
    /**
     * The reviews service.
     */
    @Autowired
    ReviewsService reviewsService;
    /**
     * The customer service.
     */
    @Autowired
    CustomerService customerService;

    /**
     * Add a review to the database and assigns the current logged-in user to it.
     * @param reviews reviews to add.
     * @return <code>true</code> if review is added successfully,
     *         <code>false</code> otherwise.
     */
    @PostMapping("/reviews")
    public boolean addReview(@RequestBody Reviews reviews){
        if(reviews!=null || reviews.getReview().isBlank())
            return reviewsService.addReview(reviews);
        return false;
    }

    /**
     * Displays all the reviews written by the customers.
     * @return list of reviews.
     */
    @GetMapping("/reviews")
    public List<Reviews> getReviews(){
    return reviewsService.getAllReviews();
    }

    /**
     * Edits a given review.
     * @param reviews review to be edited.
     * @return <code>true</code> if edit is saved successfully,
     *         <code>false</code> otherwise.
     */
    @PutMapping("/reviews")
    public boolean editReview(@RequestBody Reviews reviews){
        return reviewsService.editReview(reviews);
    }

    /**
     * Deletes the given review.
     * @param reviewId id of the review
     * @return <code>true</code> if review is deleted successfully.
     *         <code>false</code> otherwise.
     */
    @DeleteMapping("/reviews")
    public boolean deleteReview(@PathVariable int reviewId){
        return reviewsService.deleteReview(reviewId);
    }
}
