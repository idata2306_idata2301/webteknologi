package no.ntnu.app.firstaid.entity;

import javax.persistence.*;


/**
 * Represents a resource: EnrolledCourses. We store objects in the application state.
 * EnrolledCourses represents a enrolled course with all the necessary details.
 */
@Entity
public class EnrolledCourses {
    /**
     * The id of the enrolled course.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * The course id.
     */
    private int courseId;
    /**
     * The customer id.
     */
    private int customerId;
    /**
     * The date.
     */
    private String date;
    /**
     * The languages.
     */
    @Enumerated(EnumType.STRING)
    private Languages language;
    /**
     * The number of people enrolled in the course.
     */
    @Enumerated(EnumType.STRING)
    private NoOfCustomers people;
    /**
     * The time of the day.
     */
    @Enumerated(EnumType.STRING)
    private TimeOfTheDay timeOfTheDay;

    /**
     * Represents a enrolled course.
     * @param id the id of the enrolled course.
     * @param courseId the id of the course.
     * @param customerId the id of the customer.
     * @param date  of the enrolled course.
     * @param language of the enrolled course.
     * @param people of the enrolled course.
     * @param timeOfTheDay of the enrolled course.
     */
    public EnrolledCourses(int id, int courseId, int customerId, String date, Languages language,
                           NoOfCustomers people, TimeOfTheDay timeOfTheDay) {
        this.id = id;
        this.courseId = courseId;
        this.customerId = customerId;
        this.date = date;
        this.language = language;
        this.people = people;
        this.timeOfTheDay = timeOfTheDay;
    }

    /**
     * Left empty as intended.
     */
    public EnrolledCourses() {
    }

    /**
     * Returns the id of the enrolled course.
     * @return the id of the enrolled course.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the enrolled course.
     * @param id of the enrolled course.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * The id of the course.
     * @return id of the course.
     */
    public int getCourseId() {
        return courseId;
    }

    /**
     * Sets the id of the course.
     * @param courseId of the course.
     */
    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    /**
     * Returns the id of the customer.
     * @return id of the customer.
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Sets the id of the customer.
     * @param customerId of the customer.
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    /**
     * Returns the language of the enrolled course.
     * @return the language of the enrolled course.
     */
    public Languages getLanguage() {
        return language;
    }

    /**
     * Sets the language of the enrolled course.
     * @param language of the enrolled course.
     */
    public void setLanguage(Languages language) {
        this.language = language;
    }

    /**
     * Returns the date of the enrolled course.
     * @return the date of the enrolled course.
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the date of the enrolled course.
     * @param date of the enrolled course.
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Returns the number of people in the enrolled course.
     * @return the number of people in the enrolled course.
     */
    public NoOfCustomers getPeople() {
        return people;
    }

    /**
     * Sets the people in the enrolled course.
     * @param people in the enrolled course.
     */
    public void setPeople(NoOfCustomers people) {
        this.people = people;
    }

    /**
     * Returns the time of the day.
     * @return the time of the day.
     */
    public TimeOfTheDay getTimeOfTheDay() {
        return timeOfTheDay;
    }

    /**
     * Sets the time of the day of the enrolled course.
     * @param timeOfTheDay of the enrolled course.
     */
    public void setTimeOfTheDay(TimeOfTheDay timeOfTheDay) {
        this.timeOfTheDay = timeOfTheDay;
    }
}
