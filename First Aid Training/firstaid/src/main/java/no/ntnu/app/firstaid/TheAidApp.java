package no.ntnu.app.firstaid;

import no.ntnu.app.firstaid.repo.CourseRepo;
import no.ntnu.app.firstaid.repo.CustomerRepo;
import no.ntnu.app.firstaid.repo.RoleRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * Represents the main starter class for the application.
 */
@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = {CustomerRepo.class,
											 CourseRepo.class,
									         RoleRepo.class})
class TheAidApp {
	/**
	 * Starts the application itself.
	 * @param args to be added.
	 */
	public static void main(String[] args) {
		SpringApplication.run(TheAidApp.class, args);
	}

	/**
	 * Represents a configurer for the cors.
	 * @return a configurer for the cors.
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer(){
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry corsRegistry){
			corsRegistry.addMapping("/**")
					.allowedOrigins("*")
					.allowedMethods("*")
					.allowedHeaders("*")
					.allowCredentials(false)
					.maxAge(-1);
			}
		};
	}
}
