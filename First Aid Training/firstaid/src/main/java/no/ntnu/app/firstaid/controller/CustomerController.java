package no.ntnu.app.firstaid.controller;

import no.ntnu.app.firstaid.entity.Reviews;
import no.ntnu.app.firstaid.service.CustomerService;
import no.ntnu.app.firstaid.entity.Customer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
/**
 * Controller that handles the customer endpoints.
 */
@CrossOrigin(originPatterns = "*")
@RestController
public class CustomerController {
    /**
     * The customer service.
     */
    private final CustomerService customerService;

    /**
     * Creates a new instance of customerController.
     * @param customerService to be used.
     */
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * Adds a new customer to the database.
     * @param customer new customer to add.
     * @return added <code>customer</code> and <code>HTTPStatus.OK</code> if customer is added
     *          <code>HTTPStatus.BAD_REQUEST</code> if customer is not added.
     */
    @PostMapping("/customers")
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer)  {
        if (customerService.addCustomer(customer) &&
                customerService.addDefaultRoleToCustomer(customer)) {
            return new ResponseEntity<>(customer,HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Gets list of all customersList.
     * @return list of customersList.
     */
    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication.getAuthorities().toString().equalsIgnoreCase("[ROLE_ADMIN]"))
            return customerService.getAllCustomers();
        return null;
    }

    /**
     * Gets a customer by the given id.
     * @param id id of the customer
     * @return customer by the given id.
     */
    @GetMapping("/customers/{id}")
    public Customer getCustomerById(@PathVariable int id, Principal principal) {
        if(id == customerService.getCustomerIdFromUserName(principal.getName())){
            return customerService.getCustomerById(id);
        }
        return null;
    }

    /**
     * Sets role to the customer.
     * @param customerId id of the customer
     * @param roleId role to be assigned
     * @return <code>true</code> if role is assigned successfully,
     *         <code>false</code> otherwise.
     */
    @PutMapping("/customers/{customerId}/role/{roleId}")
    public boolean addRoleToCustomer(@PathVariable int customerId,
                                  @PathVariable int roleId) {
        return customerService.addRoleToCustomer(customerId, roleId);

    }

    /**
     * Deletes the given customer
     * @param id of the customer
     * @return <code>true</code> if customer is deleted successfully,
     *         <code>false</code> otherwise.
     */
    @DeleteMapping("/customers/{id}")
    public boolean deleteCustomer(@PathVariable int id, Principal principal) {
            return customerService.deleteCustomer(id);
    }

    /**
     * Get the id of the current logged-in user.
     * @param principal instance of user identity
     * @return id of the current logged-in user
     */
    @GetMapping("/current-user-id")
    public int currentUserName(Principal principal) {
       return customerService.getCustomerIdFromUserName(principal.getName());
    }

    /**
     * Updates an existing customer
     * @param customer customer to update
     * @return <code>true</code> if updated successfully,
     *         <code>false</code> otherwise.
     */
    @PutMapping("/updateCustomer")
    public boolean updateCustomer(@RequestBody Customer customer){
        return customer != null && customerService.updateCustomer(customer);
    }

    /**
     * Redirects to the home page after updating.
     * @param response HttpServletResponse object
     * @throws IOException if link is not found.
     */
    @RequestMapping("/after-update")
    void updateRedirect(HttpServletResponse response) throws IOException {
        response.sendRedirect("http://10.212.26.147:8080/logout");
    }

    /**
     * Checks whether the user is authenticated.
     * @return <code>true</code> is authenticated,
     *         <code>false</code> otherwise.
     */
    @GetMapping("/isSecured")
    public boolean isLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !authentication.getAuthorities().toString().equalsIgnoreCase("[ROLE_ANONYMOUS]");
    }

    /**
     * Gets list of reviews written by the logged-in user.
     * @param customerId id of the customer
     * @return list of reviews
     */
    @GetMapping("/customers/{customerId}/reviews")
    public  List<Reviews> getReviewsByCustomerId(@PathVariable int customerId, Principal principal){
        if(customerId == customerService.getCustomerIdFromUserName(principal.getName())){
            return customerService.getReviewsByCustomerId(customerId);
        }
        return Collections.emptyList();
    }
}