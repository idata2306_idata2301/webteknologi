package no.ntnu.app.firstaid.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a resource: Role. We store objects in the application state.
 * Role represents a customer role with all the necessary details.
 */
@Entity
public class Role {
    /**
     * The id of the role.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * The name of the role.
     */
    private String name;
    /**
     * The customers belonging to a role.
     */
    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    private List<Customer> customers;

    /**
     * Represents a role.
     * @param id of the role.
     * @param name of the role.
     */
    public Role(int id, String name) {
        this.id = id;
        this.name = name;
        customers = new ArrayList<>();
    }

    /**
     * Left blank as indended.
     */
    public Role() {
    }

    /**
     * Returns the id of the role.
     * @return id of the role.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the role.
     * @param id of the role.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the name of the role.
     * @return the name of the role.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the role.
     * @param name of the role.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns a list of customers.
     * @return a list of customers.
     */
    public List<Customer> getCustomers(){
        return this.customers;
    }
}
