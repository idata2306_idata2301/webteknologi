package no.ntnu.app.firstaid.controller;

import no.ntnu.app.firstaid.entity.EnrolledCourses;
import no.ntnu.app.firstaid.repo.EnrolledCoursesRepo;
import no.ntnu.app.firstaid.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
/**
 * Controller that handles the enrolled courses endpoints.
 */
@CrossOrigin(originPatterns = "*")
@RestController
public class EnrolledCoursesController {
    /**
     * The customer service.
     */
    @Autowired
    CustomerService customerService;
    /**
     * The enrolled courses repository.
     */
    @Autowired
    EnrolledCoursesRepo enrolledCoursesRepo;

    /**
     * Gets list of all the enrolled courses
     * @return list of enrolled courses.
     */
    public List<EnrolledCourses> getEnrolledCourses(){
        return (List<EnrolledCourses>) enrolledCoursesRepo.findAll();
    }
    /**
     * Adds a enrolled course with all the details choosen by the customer.
     * @param enrolledCourses course to be added.
     */
    @PostMapping("/save-enrolled-courses")
    public void enrollInCourse(@RequestBody EnrolledCourses enrolledCourses) {
        enrolledCoursesRepo.save(enrolledCourses);
    }

    /**
     * Gets a list of courses enrolled by the given customer id.
     * @param customerId id of the customer.
     * @return list of courses
     */
    @GetMapping("/enrolled-courses/{customerId}")
    public List<EnrolledCourses> getCourseByCustomerId(@PathVariable int customerId, Principal principal){
        if(customerId == customerService.getCustomerIdFromUserName(principal.getName())){
            return enrolledCoursesRepo.findByCustomerId(customerId);
        }
        return Collections.emptyList();
    }

    /**
     * Deletes a given enrolled course by id.
     * @param enrolledId id of the enrolled course
     * @return <code>true</code> if enrolled course is deleted successfully,
     *         <code>false</code> otherwise.
     */
    @DeleteMapping("/enrolled-courses/{enrolledId}")
    public boolean deleteEnrolledCourse(@PathVariable int enrolledId){
        EnrolledCourses enrolledCourse = enrolledCoursesRepo.findById(enrolledId).orElse(null);
        if(enrolledCourse!=null){
            enrolledCoursesRepo.delete(enrolledCourse);
            return true;
        }
        return false;
    }

}