package no.ntnu.app.firstaid.repo;

import no.ntnu.app.firstaid.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;
/**
 * Represents the customer repository.
 */
public interface CustomerRepo extends CrudRepository<Customer,Integer> {
    Optional<Customer> findCustomerByEmail(String email);
    Optional<Customer> findCustomerByFirstName(String firstName);
    Optional<Customer> findById(int id);
}
