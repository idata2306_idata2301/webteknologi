package no.ntnu.app.firstaid.entity;
import javax.persistence.*;

/**
 * Represents a resource: Course. We store objects in the application state.
 * Course represents a first aid course with all the necessary details.
 */
@Entity
public class Course {
    /**
     * The id of the course.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * The name of the course.
     */
    private String name;
    /**
     * The description of the course.
     */
    private String description;
    /**
     * The duration of the course.
     */
    private int duration;
    /**
     * The price of the course.
     */
    private int price;

    /**
     * Left empty as intended.
     */
    public Course() {
    }

    /**
     * Creates an instance of Course.
     *
     * @param id            unique id of the course.
     * @param name          name of the course.
     * @param duration      duration of the course
     * @param description   details about the course
     */
    public Course(int id, String name, int duration, String description, int price) {
        validateInt(id);
        validateString(name);
        validateInt(duration);
        validateString(description);
        validateInt(price);

        this.id = id;
        this.name = name;
        this.duration = duration;
        this.description = description;
        this.price = price;
    }

    /**
     * Returns an id of the course.
     * @return id of the course.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets an id to the course.
     * @param id id to be assigned.
     */
    public void setId(int id) {
        validateInt(id);
        this.id = id;
    }

    /**
     * Returns name of the course.
     * @return name of the course.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets a name to the course.
     * @param name name to be assigned.
     */
    public void setName(String name) {
        validateString(name);
        this.name = name;
    }

    /**
     * Returns duration of the course.
     * @return duration of the course.
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets duration to the course.
     * @param duration duration to be assigned.
     */
    public void setDuration(int duration) {
        validateInt(duration);
        this.duration = duration;
    }

    /**
     * Gets description of the course.
     * @return description of the course.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description of the course.
     * @param description description of the course.
     */
    public void setDescription(String description) {
        validateString(description);
        this.description = description;
    }

    /**
     * Returns the price of the course.
     * @return the price of the course.
     */
    public int getPrice() {
        return price;
    }

    /**
     * Sets the price of the course.
     * @param price to be set.
     */
    public void setPrice(int price) {
        validateInt(price);
        this.price = price;
    }

    /**
     * Validates that the text entered by the param
     * do NOT equal null or is empty. Else an
     * IllegalArgumentException gets thrown.
     *
     * @param textToBeVerified the text to be checked.
     */
    protected void validateString(final String textToBeVerified) {
        // If the textToBeVerified is null or is empty, throw an IllegalArgumentException
        if ((textToBeVerified == null) || (textToBeVerified.isEmpty())) {
            throw new IllegalArgumentException(
                    "The string cannot be null or empty. Received: " + textToBeVerified);
        }
    }

    /**
     * Validates that the int entered by the param
     * do NOT equals to a negative number. Else an
     * IllegalArgumentException gets thrown.
     *
     * @param intToBeVerified the int to be checked.
     */
    protected void validateInt(final int intToBeVerified) {
        // If the intToBeVerified is negative, throw an IllegalArgumentException
        if (intToBeVerified < 0) {
            throw new IllegalArgumentException(
                    "The number cannot be negative. Received: " + intToBeVerified);
        }
    }

}
