package no.ntnu.app.firstaid.security;

import no.ntnu.app.firstaid.entity.Customer;

import no.ntnu.app.firstaid.entity.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

/**
 * Represents the customer details that are needed to create a customer.
 */
public class MyCustomerDetails implements UserDetails{
    /**
     * the first name of the customer.
     */
    private String firstName;
    /**
     * The last name of the customer.
     */
    private String lastName;
    /**
     * The password of the customer.
     */
    private String password;
    /**
     * The email of the customer.
     */
    private String email;
    /**
     * List of authorities.
     */
    private List<GrantedAuthority> authorities = new ArrayList<>();

    /**
     * Represents the details of a customer.
     * @param customer details to be checked.
     */
    public MyCustomerDetails(Customer customer){
        this.firstName = customer.getFirstName();
        this.lastName = customer.getLastName();
        this.password = customer.getPassword();
        this.email = customer.getEmail();
        this.convertRoles(customer.getRoles());
    }

    /**
     * Convers the roles of the customer.
     * @param roles to be converted.
     */
    private void convertRoles(List<Role> roles) {
        authorities.clear();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return this.email ;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
