package no.ntnu.app.firstaid.service;

import no.ntnu.app.firstaid.repo.EnrolledCoursesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Represents a service to be used for the enrolled course.
 */
@Service
public class EnrolledCoursesService {
    /**
     * Represents the enrolle course repository.
     */
    @Autowired
    EnrolledCoursesRepo enrolledCoursesRepo;



}
