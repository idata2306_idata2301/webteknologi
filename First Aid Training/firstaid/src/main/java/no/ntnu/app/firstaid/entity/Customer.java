package no.ntnu.app.firstaid.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;



import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Represents a resource: Customer. We store objects in the application state.
 * Customer represents a customer with all the necessary details.
 */
@Entity
public class Customer {
    /**
     * The id of the customer.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * The first name of the customer.
     */
    @NotNull(message = "Firstname must be 2 - 30 characters!")
    @Size(min = 2, max = 30)
    private String firstName;
    /**
     * The last name of the customer.
     */
    @NotNull(message = "Lastname must be 2 - 30 characters!")
    @Size(min = 2, max = 30)
    private String lastName;
    /**
     * The password of the customer.
     */
    @NotBlank
    private String password;
    /**
     * The email and also the username of the customer.
     */
    @NotBlank
    @Email
    @Column(unique = true)
    private String email;
    /**
     * The possible roles.
     */
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "customer_roles",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private List<Role> roles = new ArrayList<>();
    /**
     * The reviews belonging to the customer.
     */
    @JsonIgnore
    @OneToMany(mappedBy = "customer")
    private List<Reviews> reviews;

    /**
     * Instance of Customer.
     * @param id id of the customer
     * @param firstName firstname of the customer
     * @param lastName lastname of the customer
     * @param password password of the customer
     * @param email email of the customer
     */
    public Customer(int id, String firstName, String lastName, String password, String email) {
        validateInt(id);
        validateString(firstName);
        validateString(lastName);
        validateString(password);
        validateString(email);
        
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        if(emailValidator(email) && !email.isEmpty() && !email.isBlank()){
            this.email = email;
        }else{
            this.email = " ";
        }
        this.reviews = new ArrayList<>();
    }

    /**
     * Left empty as intended.
     */
    public Customer() {
    }

    /**
     * Returns the customers password.
     * @return the customers password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the customers password.
     * @param password to be set.
     */
    public void setPassword(String password) {
      this.password = password;
    }

    /**
     * Returns the customers id.
     * @return the customers id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the customer.
     * @param id of the customer to be set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the first name of the customer.
     * @return the first name of the customer.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of the customer.
     * @param firstName to be set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * Returns the last name of the customer.
     * @return the last name of the customer.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of the customer.
     * @param lastName of the customer.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * Returns the email of the customer.
     * @return the email of the customer.
     */
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        if(emailValidator(email)){this.email = email;}else{this.email="";}
    }

    /**
     * Returns the roles.
     * @return the roles.
     */
    public List<Role> getRoles(){ return roles;}

    /**
     * Sets the roles.
     * @param roles to be set.
     */
    public void setRoles(List<Role> roles){ this.roles = roles;}

    /**
     * Returns the reviews.
     * @return the reviews.
     */
    public List<Reviews> getReviews() {
        return reviews;
    }

    /**
     * Sets the reviews.
     * @param reviews to be set.
     */
    public void setReviews(List<Reviews> reviews) {
        this.reviews = reviews;
    }

    /**
     * Adds a role to the customer.
     * @param role to be added.
     */
    public void addRoleToCustomer(Role role) { this.roles.add(role);}

    /**
     * Adds reviews to the customer.
     * @param reviews to be added.
     */
    public void addReviewToCustomer(Reviews reviews){
        this.reviews.add(reviews);
    }

    /**
     * Validates the given email.
     * @param email email to be validate
     * @return <code>true</code> if email is valid,
     *         <code>false</code> otherwise.
     *
     */
    public boolean emailValidator(String email) {
        String regexPattern = "^(.+)@(\\S+)$";
        return Pattern.compile(regexPattern)
                .matcher(email)
                .matches();
    }


    /**
     * Validates that the text entered by the param
     * do NOT equal null or is empty. Else an
     * IllegalArgumentException gets thrown.
     *
     * @param textToBeVerified the text to be checked.
     */
    protected void validateString(final String textToBeVerified) {
        // If the textToBeVerified is null or is empty, throw an IllegalArgumentException
        if ((textToBeVerified == null) || (textToBeVerified.isEmpty())) {
            throw new IllegalArgumentException(
                    "The string cannot be null or empty. Received: " + textToBeVerified);
        }
    }

    /**
     * Validates that the int entered by the param
     * do NOT equals to a negative number. Else an
     * IllegalArgumentException gets thrown.
     *
     * @param intToBeVerified the int to be checked.
     */
    protected void validateInt(final int intToBeVerified) {
        // If the intToBeVerified is negative, throw an IllegalArgumentException
        if (intToBeVerified < 0) {
            throw new IllegalArgumentException(
                    "The number cannot be negative. Received: " + intToBeVerified);
        }
    }
}
