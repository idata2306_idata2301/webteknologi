# First Aid Training
## Intention with the project

This is a combined project in the courses IDATA2301 - Web-technology and IDATA2306 - Application Development.
The main idea behind the project is to make students create a website after certain criteria given by the teacher in the course, and connect the website to a backend application with a database.
Web-technology focuses on the frontend using HTML, CSS and JavaScript for developing a dynamic website, and the 
application development course focuses on a backend application and database.
The site must also be running on a live server that is accessible within the NTNU network, either by VPN or local connection at the university. 

## Installation

1\. To be able to run the appication, you will need the **Java Runtime Environment** and you can install it from the terminal. The application also do require a **MySQL-server** to be able to initialize and save data. If you do not have a MySQL-server, you can find guides to set it up [here](https://dev.mysql.com/doc/mysql-getting-started/en/). Open the terminal and write "**sudo apt install default-jre**" and click Enter. If prompted to type the password, please do so to ake sure the installer gets the permission it needs to install the package. Please make sure that the folder containing the .jar file has the correct permissions. 

2\. Download the *.jar file and save it on a desired location.

4\. Open Terminal and type:

**java -jar -Dserver.port=8080 pathToJar/*.jar -spring.datasource.username=datasourceUser -spring.datasource.password=datasourcePassword -spring.datasource.url=jdbc:mysql://ipAdress:databasePort/databaseName **

IMPORTANT! Please make sure that:

* **server.port** is set to the desired port. Please do avoid using ports below 1024 due to that those ports are defined as privileged ports and normal users are not allowed to run servers on them accourding to: [W3.org](https://www.w3.org/Daemon/User/Installation/PrivilegedPorts.html)
* **spring.datasource.username** is set to the database user with the correct access.
* **spring.datasource.password** is set to the password of the database user with the correct access.
* **spring.datasource.url** must be adapted to your configuration. Change **ipAdress** with the database servers ip-adress, change **databasePort** to the database servers port, and change **databaseName** to the database created for the application.
* **databaseName** is set to the correct database name. Please do avoid capital letters in the database name.


5\. If everything is allright, you should see the application starting.

6\. If the application refuses to start, open the database and find the "role"-table. Please enter:


|   | **id**    |  **name**       |
|---|-----------|-----------------|
|   |  **1**    |  **ROLE_USER**  |
|   |  **2**    |  **ROLE_ADMIN** |




7\. Remember to save.

8\. Start the application again.

## Crontab

### 1. To make sure the application automaticly starts when the server itself starts, open terminal, type "crontab -e" and click enter. Insert the following:

**@reboot java -jar -Dserver.port=8080 pathToJar/*.jar -spring.datasource.username=userNameToDataSource -spring.datasource.password=passwordToDatasource -spring.datasource.url=jdbc:mysql://ipAdress:databasePort/databaseName*

Please do remember to save!

### 2. Remember to change the properties to fit your needs. 
* Insert the server port to be used instead of **8080**

* Insert the path to the jar file to be used instead of "**pathToJar**".

* Insert the username for the database user instead of "**usernameToDataSource**"

* Insert the password for the database user instead of "**passwordToDatasource**"

* Insert the network port to the desired port instead of "**8080**"

* Insert the database server hostname or ip-adress instead of "**ipAdress**"

* Insert the database servers sql port instead of "**databasePort**"

* Insert the database scheme name instead of "**databaseName**"
